# Authorea TeX Fix

Apply string substitutions and paragraph substitutions to Authorea's TeX output to make document compilable in pdflatex.

## Getting started

Download the script, grant run permissions (`chmod a+x authorea_tex_process`) and run:

```
authorea_tex_process input_file.tex output_file.tex
```

And compile with `pdflatex` or other $\LaTeX$ compilers (`xelatex` etc. currently untested)

## License
GPL3

## Project Status
No immediate plans to work on this, but in theory it should be adaptable to ensure wider compatibility, and catching more Authorea -> TeX edgecase issues. Accepting contributions!
